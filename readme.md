# MSSQL test

this small test project aims to evaluate the speed of the queries using
raw mssql, without additional layers in order to know possible optimizations
on the project Sofy

the follow code will run on any node.js capable system operative, by default
mssql uses [tedious][1] is a pure javascript implementation of the [TDS protocol][2] and can use [msnodesqlv8][3] which is a native implementation but only available on Windows

this code was intended to evaluate a single request, doesn't have in account parallel request or concurrent ones.

It make use of the most recent package mssql and also was wrote using node 7.9.0

The tests use a single query, extracted from the api call `/api/test/queryBig`

    SELECT * FROM "sph_audittrail" ORDER BY id OFFSET 0 ROWS FETCH NEXT 500 ROWS ONLY

## Environment vars

|name|description|
|---|---|
|NO_LOG|if set the log/* files will be not written|
|CACHE_TYPE|Cache type, allowed values `memory`, `redis` or `none`. by default `none`|
|COUNT|How many times the query will be executed (query_big.js only). By default `100`|

## Cache types

- `memory`, will store the cache on the same process memory
- `redis`, will store the cache on redis server
- `none`, no cache for the queries


## How to use

1. Clone this repository

    `$ git clone https://gitlab.com/eddsuarez89/mssql sofy-test`

2. Install node dependencies

    `$ npm install`

3. Execute the test

    `$ npm start`

The test take some time, after his execution a couple of files will be written on the folder `log` on the root

- log/query_big_msnodesqlv8.log
- log/query_big_tedious.log

if this code is got executed on any system different from Windows the file `log/query_big_msnodesqlv8.log` won't be created

4. Get the avg time for each file

    `$ npm run result -- log/query_big_msnodesqlv8.log`

    `$ npm run result -- log/query_big_tedious.log`

the results will be printed on the stdout

[1]: https://github.com/tediousjs/tedious
[2]: http://msdn.microsoft.com/en-us/library/dd304523.aspx
[3]: https://www.npmjs.com/package/msnodesqlv8
