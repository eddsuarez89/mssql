const FileReader = require('line-by-line');

const result = (filepath) => {
  const reader = new FileReader(filepath);
  const results = {};

  reader.on('line', onLine(results));
  reader.on('end', onEnd(results));
  reader.on('error', onError);
};

const onError = (err) => {
  console.error(err);
  process.exit(1);
};

const onEnd = (results) => () => {
  for (label in results) {
    const avg = results[label].sum / results[label].count;
    console.log('Results', label, avg.toFixed(2));
  }
  process.exit(0);
};

const onLine = (results) => (line) => {
  const [label, valueStrig] = line.split(':');
  results[label] = results[label] || {count: 0, sum: 0};
  results[label].count+= 1;
  results[label].sum += parseFloat(valueStrig);
};

module.exports = result;

if (require.main === module) {
    result(process.argv[2]);
}
