const { clearLogs, setLogFile } = require('./lib');
const { query, connect, close } = require('./lib/sql_cache');

const configTedious = require('../config/mssql_tedious.json');
const configMSN = require('../config/mssql_msnodesqlv8.json');

const isWin = /^win/.test(process.platform);
const bigQuery = 'SELECT * FROM "sph_audittrail" ORDER BY id OFFSET 0 ROWS FETCH NEXT 500 ROWS ONLY';
const runCount = parseInt(process.env.COUNT || 100);

const onError = (error) => {
  console.error(error);
  process.exit(1);
};

const onGood = () => {
  process.exit(0);
};

const timeLabel = {
  conn: 'conn',
  query: 'bigQuery',
  queryCached: `bigQuery (same connection)`
};

const runTest = async (config) => {
  console.time(timeLabel.conn);
  await connect(config);
  console.timeEnd(timeLabel.conn);

  console.time(timeLabel.query);
  await query(bigQuery);
  console.timeEnd(timeLabel.query);

  for (let i = 1; i <= runCount; i++) {
    console.time(`${timeLabel.queryCached}`);
    await query(bigQuery);
    console.timeEnd(`${timeLabel.queryCached}`);
  }
  await close();
};

(async () => {
  await clearLogs();
  setLogFile('query_big_tedious');
  await runTest(configTedious);
  if (isWin) {
    setLogFile('query_big_msnodesqlv8');
    await runTest(configMSN);
  }

})().catch(onError).then(onGood);
