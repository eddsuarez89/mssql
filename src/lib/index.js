const path = require('path');
const fs = require('fs-extra');

const LOG_DIR = path.join(__dirname, '..', '..', 'log');
const NO_LOG = !!process.env.NO_LOG;

module.exports.clearLogs = async () => {
  await fs.remove(LOG_DIR);
  await fs.ensureDir(LOG_DIR);
};

module.exports.setLogFile = (name) => {
  if (NO_LOG) return;
  const writer = fs.createWriteStream(path.join(LOG_DIR, `${name}.log`));
  process.stdout.write = writer.write.bind(writer);
};
