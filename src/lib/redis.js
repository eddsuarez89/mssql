const redis = require('redis');

const config = require('../../config/redis.json');

module.exports = redis.createClient(config.port, config.server, config);


