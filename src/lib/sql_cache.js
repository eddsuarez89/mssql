const mssql = require('mssql');
const stow = require('stow');
const MemoryBackend = require('stow/backends/memory');
const RedisBackend = require('stow/backends/redis');

let cache = {
  set: (args, cb) => cb(null, false),
  get: (args, cb) => cb(null, false)
};


switch (process.env.CACHE_TYPE) {
  case 'redis':
    cache = stow.createCache(RedisBackend, {
      client: require('./redis')
    });
    break;
  case 'memory':
    cache = stow.createCache(MemoryBackend, {});
    break;
  case 'none':
  default:
    break;
}

const hash = (value) => Buffer.from(value).toString('base64');

const cacheGet = (rawKey) => new Promise((resolve, reject) => {
  const key = hash(rawKey);
  cache.get(key, (err, result) => {
    if (err) reject(err);
    else resolve(result);
  })
});

const cacheSet = (rawKey, data) => new Promise((resolve, reject) => {
  const key = hash(rawKey);
  cache.set({key, data}, (err) => {
    if (err) reject(err);
    else resolve(true);
  })
});

const query = async (sql) => {
  const cached = await cacheGet(sql);
  if (!cached) {
    const ps = new mssql.PreparedStatement();
    await ps.prepare(sql);
    const result = await ps.execute();
    await ps.unprepare();
    await cacheSet(sql, result);
    return result;
  }

  return cached
};

module.exports = {
  query,
  connect: async (args) => await mssql.connect(args),
  close: mssql.close
};