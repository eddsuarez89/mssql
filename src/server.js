const express = require('express');
const session = require('express-session');
const wrap = require('express-async-wrapper');
const ConnectRedis = require('connect-redis')(session);

const {connect, query} = require('./lib/sql_cache');
const config = require('../config/mssql_tedious.json');
const configSession = require('../config/session.json');

const app = express();

const bigQuery = wrap(async (req, res) => {
  const result = await query`SELECT * FROM "sph_audittrail" ORDER BY id OFFSET 0 ROWS FETCH NEXT 500 ROWS ONLY`;
  res.json(result);
});

const hello = wrap(async (req, res) => {
  res.json("hello");
});

configSession.store = new ConnectRedis({client: require('./lib/redis')});

app.use(session(configSession));

const init = async () => {
  await connect(config);
  app.get('/api/test/queryBig', bigQuery);
  app.get('/api/test/hello', hello);
  app.listen(8080);
};

const onError = (error) => {
  console.error(error);
  process.exit(1);
};

init().catch(onError);